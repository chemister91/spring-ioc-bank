package ioc;

public enum AccountType {
    CREDITCARD("Кредитная карта"),
    DEBITCARD("Дебетовая карта"),
    LOAN("Кредит"),
    DEPOSIT("Депозит");

    private String title;

    AccountType(String title) {
        this.title = title;
    }

    public String getTitle() {
        return title;
    }
}
