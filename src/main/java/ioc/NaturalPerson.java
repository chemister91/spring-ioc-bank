package ioc;

public class NaturalPerson extends Client {
    private String clientIDCard;

    public String getClientIDCard() {
        return clientIDCard;
    }

    public NaturalPerson(String fullName, String clientIDCard) {
        super(fullName, ClientType.NATURAL);
        this.clientIDCard = clientIDCard;
    }
}
