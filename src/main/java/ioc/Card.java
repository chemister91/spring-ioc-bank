package ioc;

import java.util.concurrent.ThreadLocalRandom;

public class Card extends Account {
    private String cardNumber;
    private int accNo;

    private void generateCardNumber(){
        cardNumber =  String.format("%d010%d", 546900 + accNo, ThreadLocalRandom.current().nextInt(1, Integer.MAX_VALUE));
    }

    public String getCardNumber() {
        if (cardNumber == null){
            generateCardNumber();
        }
        return cardNumber;
    }

    public void setCardNumber(String cardNumber) {
        this.cardNumber = cardNumber;
    }

    public Card(AccountType accountType, double sum, int accNo) {
        super(accountType, sum);
        this.accNo = accNo;
    }

}
