package ioc;

public class Account {
    public Account(AccountType accountType, double sum) {
        this.accountType = accountType;
        this.sum = sum;
    }

    public AccountType getAccountType() {
        return accountType;
    }

    public void setAccountType(AccountType accountType) {
        this.accountType = accountType;
    }

    public double getSum() {
        return sum;
    }

    public double getRealSum() {
        switch (accountType) {
            case CREDITCARD:
            case LOAN:
                return -sum;
            case DEBITCARD:
            case DEPOSIT:
                return sum;
            default:
                return 0;
        }
    }

    public void setSum(double sum) {
        this.sum = sum;
    }

    private AccountType accountType;
    private double sum;
}
